from django.test import TestCase

from .models import Song

from .util import search_string_to_yt_url


class QuestionMethodTests(TestCase):

    def test_search_string_to_yt_url(self):
        yturl = search_string_to_yt_url('chicken attack')
        try:
            self.assertEqual(yturl, 'www.youtube.com/watch?v=miomuSGoPzI')
        except Exception as e:
            try:
                self.assertEqual(yturl, 'www.youtube.com/watch?v=miomuSGoPzI')
            except Exception as e:
                raise e
