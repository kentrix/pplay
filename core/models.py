from django.db import models

class Song(models.Model):

    def __str__(self):
        return self.title

    song_id = models.IntegerField()
    title = models.CharField(max_length = 256)
    origin_url = models.CharField(max_length = 1024)
    file_path = models.CharField(max_length = 512, primary_key = True)
    file_size = models.FloatField()
