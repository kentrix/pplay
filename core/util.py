import re
import urllib3
import threading
import logging

from urllib import parse
from django.conf import settings
from .pplay_mpd import Pplay_MPDClient
from subprocess import call

YT_QUERY_URL_PREFIX = 'www.youtube.com/results?search_query='
YT_QUERY_RESULT_MATCH_RE = r'data-video-ids=\".{11}\"'
YT_VIDEO_PREFIX = 'www.youtube.com/watch?v='

logger = logging.getLogger(__name__)

class NotAValidURL(Exception):
    pass

def search_string_to_yt_url(search_string):
    pool = urllib3.PoolManager()
    result = pool.request('GET', YT_QUERY_URL_PREFIX + parse.quote(search_string))
    if result.status == 200:
        video_id = yt_html_get_first_video(result.data.decode('utf-8'))
        print('video id is ' + video_id)
        if video_id != None:
            return YT_VIDEO_PREFIX + video_id

    return None


def yt_html_get_first_video(html_data):
    match = re.search(YT_QUERY_RESULT_MATCH_RE, html_data)
    if match != None:
        return match.group().split('"')[1]
    else:
        return None

def validate_url(url):
    pass

def get_extension(url):
    return url.split('.')[-1]

def gen_song_file_name():
    return gen_random_string(10)

def gen_random_string(length):
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in range(length))

def obtain_and_add_origin_url(connection, songs):
    for song in songs:
        origin_url = connection.sticker_list('song', song['file']).get('origin_url')
        if(origin_url):
            song['origin_url'] = origin_url

    return songs

