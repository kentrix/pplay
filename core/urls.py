from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^submit$', views.submit, name='submit'),
    url(r'^post$', views.post, name='post'),
    url(r'^songs$', views.songs, name='songs'),
    url(r'^control$', views.control, name='control'),
    url(r'^$', views.index, name='index')
]
