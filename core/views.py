from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from .pplay_mpd import Pplay_MPDClient
from django.conf import settings
from .util import obtain_and_add_origin_url, search_string_to_yt_url

from .worker.youtube_dl_worker import youtube_dl_thread
import logging

logger = logging.getLogger(__name__)

def index(request):
    template = loader.get_template('core/index.html')
    with Pplay_MPDClient().connect(settings.MPD_HOST, settings.MPD_PORT) as mpdc:
        logger.debug('Connection to MPD at HOST: '+ str(settings.MPD_HOST) +' Port: '+ str(settings.MPD_PORT))
        playlist = mpdc.playlistinfo()
        current_song = mpdc.currentsong()
        if 'pos' in current_song:
            current_song_pos = current_song['pos']
        else:
            current_song_pos = '-1'
        playlist = obtain_and_add_origin_url(mpdc, playlist)

    context = {
        'playlist': playlist,
        'current_song_pos' : current_song_pos
    }
    return HttpResponse(template.render(context, request))

def songs(request):
    template = loader.get_template('core/songs.html')
    with Pplay_MPDClient().connect(settings.MPD_HOST, settings.MPD_PORT) as mpdc:
        logger.debug('Connection to MPD at HOST: '+ str(settings.MPD_HOST) +' Port: '+ str(settings.MPD_PORT))
        songs = mpdc.lsinfo()
        songs = obtain_and_add_origin_url(mpdc, songs)

    context = {
            'songs' : songs,
    }
    return HttpResponse(template.render(context, request))


def submit(request):
    template = loader.get_template('core/submit.html')
    context = dict()
    return HttpResponse(template.render(context, request))

def post(request):
    if 'url_submit' in request.POST:
        url = request.POST['url_submit']
        logger.debug('User message: %s' % url)
    elif 'string_submit' in request.POST:
        search_string = request.POST['string_submit']
        logger.debug('User message: %s' % search_string)
        dl_thread = youtube_dl_thread(query_string = search_string)
        dl_thread.start()
    elif 'addsong' in request.POST:
        song_to_add = request.POST['addsong']
        with Pplay_MPDClient().connect(settings.MPD_HOST, settings.MPD_PORT) as mpdc:
            mpdc.add(song_to_add)

    return HttpResponseRedirect(reverse('index', args=()))

def control(request):
    try:
        action = request.POST['control']
        logger.debug('User message: %s' % action)
        with Pplay_MPDClient().connect(settings.MPD_HOST, settings.MPD_PORT) as mpdc:
            if action == 'play':
                mpdc.play()
            elif action == 'pause':
                mpdc.pause()
            elif action == 'stop':
                mpdc.stop()
            elif action == 'next':
                pos = int(mpdc.currentsong()['pos'])
                if pos == (mpdc.playlistinfo()[-1]['pos']):
                    mpdc.stop()
                else:
                    mpdc.play(pos+1)
            else:
                logger.critical('Unknown action: ' + action)

    except KeyError as e:
        logger.critical('No post data')

    if 'jump' in request.POST:
        with Pplay_MPDClient().connect(settings.MPD_HOST, settings.MPD_PORT) as mpdc:
            mpdc.play(int(request.POST['jump']))

    if 'remove' in request.POST:
        with Pplay_MPDClient().connect(settings.MPD_HOST, settings.MPD_PORT) as mpdc:
            mpdc.delete(int(request.POST['remove']))

    if 'purge' in request.POST:
        with Pplay_MPDClient().connect(settings.MPD_HOST, settings.MPD_PORT) as mpdc:
            mpdc.stop()
            mpdc.clear()

    return HttpResponseRedirect(reverse('index', args=()))
