import os.path
import logging
import youtube_dl
import tempfile
import threading

from time import sleep
from os import listdir, chmod
from django.conf import settings
from ..pplay_mpd import Pplay_MPDClient
from subprocess import run
from shutil import move, rmtree
from ..pending_song import pending_list
from ..util import search_string_to_yt_url

logger = logging.getLogger(__name__)

class youtube_dl_thread(threading.Thread):
    def __init__(self, url=None, query_string=None):
        threading.Thread.__init__(self)
        self.url = url
        self.query_string = query_string

    def run(self):
        if self.url == None and self.query_string == None:
            raise Exception("Neither url or query string specified")

        if self.query_string:
            self.url = search_string_to_yt_url(self.query_string)
            logger.debug('youtube-dl query: %s to url: %s' % (self.query_string, self.url))

        logger.critical('youtube-dl thread start: URL: ' + self.url)
        temp_path = tempfile.mkdtemp(prefix = 'pplay')
        tmpl_with_path = os.path.join(temp_path, "%(title)s.%(ext)s")
        call_args = ['--extract-audio', '--audio-format', 'mp3', '--output', tmpl_with_path, self.url]

        try:
            run([settings.YOUTUBE_DL_PATH] + call_args)
        except Exception as e:
            rmtree(temp_path)
            logger.critical("Download URL: " + self.url + " Failed!")
            raise(e)

        logger.critical('youtube-dl download finished: URL: ' + self.url)
        dled_file = listdir(temp_path)
        if len(dled_file) != 1:
            raise Exception("Unexpected number of files in " + temp_path)
            rmtree(temp_path)
        else:
            dled_file = dled_file[0]

        move_to = dled_file.replace(' ', '_')

        move(os.path.join(temp_path, dled_file), os.path.join(settings.MUSIC_PATH, move_to))

        with Pplay_MPDClient().connect(settings.MPD_HOST, settings.MPD_PORT) as mpdc:
            mpdc.update()
            mpdc.idle('update')
            #Arbitrary sleep to made sure the song made into the database
            sleep(0.2)
            mpdc.add(move_to)
            mpdc.sticker_set('song', move_to, 'origin_url', self.url)

        logger.critical("Thread task done")


    def run_by_api(self):
        logger.critical('youtube-dl thread start: URL: ' + self.url)
        temp_path = tempfile.mkdtemp(prefix = 'pplay')
        ytdl_param = {
                      'outtmpl' : os.path.join(temp_path, '%(title)s.%(ext)s'),
                      'format': 'bestaudio/best',
                      'postprocessors': [{
                        'key': 'FFmpegExtractAudio',
                        'preferredcodec': 'mp3',
                        'preferredquality': '192',}],
                     }

        with youtube_dl.YoutubeDL(ytdl_param) as dler:
            dler.download([self.url])
        
