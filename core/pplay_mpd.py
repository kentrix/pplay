from .mpd.base import MPDClient, ConnectionError

#@mpd_command_provider
class Pplay_MPDClient(MPDClient):
    connected = False

    def __enter__(self):
        if not self.connected:
            raise ConnectionError("Not connected!")
        return self

    def __exit__(self, t, v, tb):
        self.disconnect()

    def connect(self, host, port, timeout=None):
        try:
            super(Pplay_MPDClient, self).connect(host, port, timeout)
            self.connected = True
        except Exception as e:
            print(e)

        return self
